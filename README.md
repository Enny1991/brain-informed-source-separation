# Brain-informed speech separation 
This is the code relative to the paper [Brain-informed speech separation (BISS) for enhancement of target speaker in multitalker speech perception](https://www.sciencedirect.com/science/article/pii/S1053811920307680).
If you use any part of this code please cite the paper! 

## Structure
You can use the notebook [prepare_dataset](prepare_dataset.ipynb) to generate an h5 dataset from a bunch of wav files that can be used then to train/test the models.

The model receives in input a mixture waveform + an hint (envelope) and outputs the cleaned source corresponding to the 
given envelope. The model input and output audio signals are in the time domain, the separation is done in the 
frequency domain but the optimization is done in the time domain. To allows for this the stft and istft are implemented 
in the model (see [model_stft](model_stft.py)). 

The datasets for training/testing are managed directly by the Loaders in [data_utils](data_utils.py), 
the loaders take care of loading wav files in the h5 file that you give them. They create mixtures on the fly and 
compute the envelope of the desired signal. There are two Loaders one to create mixtures of speech vs speech and one to 
create mixtures of speech vs noise.

The speech extraction network is implemented in [model_stft](model_stft.py) and is called
InfoComMask.

The experiments can be instantiated with the script [do_exp_stft](do_exp_stft.py), check in the script all the arguments 
that can be set from the cmd line
```bash
python do_exp_stft.py --epochs 20 --exp_name ...
```

The code uses tensorflow to log training/testing information (see [Logger](logger.py)). This information can be checked 
live with tensorboard. 

### Speech Envelopes 
The [data_utils](data_utils.py) shows two ways in which one can compute envelopes from the speech depending 
on which one was used during the Auditory Attention Decoding (AAD).

### Evaluation scripts
The scripts full_evaluation* can run the full evaluations of all the models one wants to compare on a precise dataset.
The output of these scripts are csv files with all the results.

The notebooks evaluation* are used to plot the overall results using the csv files produced at the previous step.

