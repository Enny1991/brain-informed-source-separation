import csv
import json
import pickle as pkl


import numpy as np
import torch
from sep_eval import sep_eval as se
from tqdm import tqdm

from model_stft import InfoComMask

CONST = 10 * np.log10(np.exp(1))


class Evaluator(object):
    def __init__(self):
        
        self.model = None
        self.models = []
        
        self.measures = []
        self.name_measure = []
        
        self.datasets = []
        self.name_dataset = []
        self.corrs = []
        self.sub_id = []
        self.trial_id = []
        self.corrs_w = []
        self.hi = []

    def add_model(self, name):
        self.models.append(name)
    
    def add_measure(self, measure, name=None):
        if name is None:
            name = 'measure{}'.format(len(self.measures))
        self.name_measure.append(name)
        self.measures.append(measure)
    
    def add_dataset(self, mix, hint, s1, name=None, corrs_w=None, corrs=None, trial_id=None, sub_id=None, hi=None,
                    mode='env'):
        if name is None:
            name = 'model{}'.format(len(self.models))
        if corrs is not None:
            self.corrs.append(corrs)
        else:
            self.corrs.append(np.zeros((len(mix))))

        if corrs_w is not None:
            self.corrs_w.append(corrs_w)
        else:
            self.corrs_w.append(np.zeros((len(mix))))

        if trial_id is not None:
            self.trial_id.append(trial_id)
        else:
            self.trial_id.append(np.zeros((len(mix))))

        if sub_id is not None:
            self.sub_id.append(sub_id)
        else:
            self.sub_id.append(np.zeros((len(mix))))

        if hi is not None:
            self.hi.append(hi)
        else:
            self.hi.append(np.zeros((len(mix))))

        self.name_dataset.append(name)
        self.datasets.append({'mix': mix, 'hint': hint, 's1': s1, 'mode': mode})

    @staticmethod
    def single_test(model, mix, env, mode):
        # doing batches of 4
        batch_size = 2
        out = []
        for i in tqdm(range(0, len(mix), batch_size)):
            _out = model([mix[i:i + batch_size].cuda(), env[i:i + batch_size].cuda()], branch=mode)
            _out = _out.cpu().data.numpy()
            out.append(_out)
        out = np.vstack(out)
        return out

    @staticmethod
    def load_info_model(load, base_dir='/Data/Dropbox/PhD/Projects/brain-informed-source-separation'):
        json_dir = base_dir + '/logs2/' + load
        with open(json_dir + '/architecture.json', 'r') as fff:
            p = json.load(fff)
            load_path = json_dir

            model = InfoComMask(n_fft=p['nfft'], kernel=(p['kf'], p['kt']), causal=p['causal'],
                                layers=p['layers'], stacks=p['stacks'], verbose=False)

            model.load_state_dict(torch.load(load_path + '/model_weights.pt'))
            model = model.cuda()
            _ = model.eval()
            return model, p

    def evaluate(self, file_path='./out.csv', mode='w'):
        
        with open(file_path, mode) as csv_file:
            file_writer = csv.writer(csv_file, delimiter=',')
            header = ['model', "nfft", "hop", "kernel1", "kernel2", "stacks", "nspk", "layers", "causal", "spkprob",
                      "tr_len", "te_len", "max_len", "noise_train", "frame_train", "noise_test", "frame_test",
                      "dataset_path", "dataset_name", "corrs", "corrs_w", "trial_id", "sub_id", "hi"] + self.name_measure
            file_writer.writerow(header)
            for model in self.models:
                model_row = [model]
                
                print("Loading {}".format(model))
                _model, _p = self.load_info_model(model)

                for k, v in _p.items():
                    model_row.append(v)

                for dataset, name_dataset, corrs, \
                    corrs_w, trial_id, sub_id, hi in zip(self.datasets, self.name_dataset, self.corrs, self.corrs_w, self.trial_id, self.sub_id, self.hi):
                    dataset_row = [name_dataset]
                    _out = self.single_test(_model, dataset['mix'], dataset['hint'], dataset['mode'])
                    _s1 = dataset['s1'].cpu().data.numpy()
                    collected_measures = []
                    for measure, name_measure in zip(self.measures, self.name_measure):
                        collected_measures.append(measure(_out, _s1))
                    # separate
                    res = np.zeros((len(self.measures), len(collected_measures[0]), 6))
                    for j, m in enumerate(collected_measures):
                        for i, val in enumerate(m):
                            res[j, i, 0] = val
                            res[j, i, 1] = corrs[i]
                            res[j, i, 2] = corrs_w[i]
                            res[j, i, 3] = trial_id[i]
                            res[j, i, 4] = sub_id[i]
                            res[j, i, 5] = hi[i]
                    for i in range(res.shape[1]):
                        _t = []
                        _t.append(res[0, i, 1])  # corr
                        _t.append(res[0, i, 2])  # corr w
                        _t.append(res[0, i, 3])  # trial id
                        _t.append(res[0, i, 4])  # sub id
                        _t.append(res[0, i, 5])  # hi
                        for j in range(res.shape[0]):
                            _t.append(res[j, i, 0])
                        file_writer.writerow(model_row + dataset_row + _t)
                del _model  # for RAM


def main():
    # Real data
    real_data = pkl.load(open('prepared_speech_vs_speech_eeg_v5.pkl', 'rb'))
    s1 = torch.from_numpy(real_data['s1'].astype('float32'))
    mix = torch.from_numpy(real_data['mix'].astype('float32'))
    env = torch.from_numpy(real_data['env'].astype('float32'))

    corrs = real_data['corrs']
    corrs_w = real_data['corrs_w']

    trial_id = real_data['trial_id']
    sub_id = real_data['sub_id']
    hi = real_data['hi']

    print("REAL test MIX: {}".format(mix.shape))
    print("REAL test ENV: {}".format(env.shape))
    print("REAL test s1: {}".format(s1.shape))

    # ACTUAL COMPUTE
    evaluator = Evaluator()

    # NC
    evaluator.add_model('201908214513_ICM_noise_40_32_std_125_sep_NC')

    # C
    evaluator.add_model('201908052205_ICM_noise_50_40_std_125_sep_C')

    evaluator.add_dataset(mix, env, s1, name='recon test env', mode='env', corrs=corrs, corrs_w=corrs_w,
                          trial_id=trial_id, sub_id=sub_id, hi=hi)

    evaluator.add_measure(se.sdr, name='sdr')
    evaluator.add_measure(se.stoi, name='stoi')
    evaluator.evaluate(file_path='./final_evaluation_eeg_sp_vs_sp_with_corrs_v7.csv')


if __name__ == "__main__":
    main()
