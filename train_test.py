from torch.autograd import Variable
import torch
import sdr
from sdr import batch_SDR_torch
import numpy as np
from tqdm import tqdm

CONST = 10 * np.log10(np.exp(1))


def sSDR(output, clean):
    return torch.mean(sdr.calc_sdr_torch(output, clean))


# noinspection PyArgumentList,PyUnboundLocalVariable
def train(model, train_loader, optimizer, epoch, args):
    model.train()
    train_loss = 0.

    train_loader.dataset.set_noise(args.noise_train)
    train_loader.dataset.set_frame_len(args.frame_train)
    data_gen = tqdm(enumerate(train_loader), desc=f"Train epoch: {epoch:3d} || SDR: {-np.Inf:4.4f}",
                    total=len(train_loader))

    for batch_id, val in data_gen:
        feat = Variable(val[0]).contiguous()
        spk1 = Variable(val[1]).contiguous()
        if not args.use_pit:
            env = Variable(val[2]).contiguous()
        else:
            spk2 = Variable(val[2]).contiguous()
            env = Variable(val[3]).contiguous()

        if args.cuda:
            feat = feat.cuda()
            spk1 = spk1.cuda()
            if args.use_pit:
                spk2 = spk2.cuda()
            env = env.cuda()

        optimizer.zero_grad()

        # SDR as objective
        if not args.use_pit:
            clean = spk1
            recon = model([feat, env], args.noise_train)
            loss = - sSDR(recon, clean)
        else:
            clean1 = spk1
            clean2 = spk2
            recon1, recon2 = model([feat, env], args.noise_train)
            recon = torch.cat([recon1.unsqueeze(1), recon2.unsqueeze(1)], 1)
            clean = torch.cat([clean1.unsqueeze(1), clean2.unsqueeze(1)], 1)
            loss = - torch.mean(batch_SDR_torch(recon, clean, args.cuda))

        loss.backward()
        train_loss += loss.data.item() * CONST

        optimizer.step()

        data_gen.set_description(f"Train epoch: {epoch:3d} || SDR: {-train_loss / (batch_id + 1):4.4f}")

    return train_loss / (batch_id + 1)


# noinspection PyUnboundLocalVariable,PyArgumentList
def test(model, validation_loader, epoch, args):
    model.eval()
    validation_loss = 0.
    batch_id = 0

    validation_loader.dataset.set_noise(args.noise_test)
    validation_loader.dataset.set_frame_len(args.frame_test)
    data_gen = tqdm(enumerate(validation_loader), desc=f"Test  epoch: {epoch:3d} || SDR: {-np.Inf:4.4f}",
                    total=len(validation_loader))

    for batch_id, val in data_gen:
        feat = Variable(val[0]).contiguous()
        spk1 = Variable(val[1]).contiguous()
        if not args.use_pit:
            env = Variable(val[2]).contiguous()
        else:
            spk2 = Variable(val[2]).contiguous()
            env = Variable(val[3]).contiguous()

        if args.cuda:
            feat = feat.cuda()
            spk1 = spk1.cuda()
            if args.use_pit:
                spk2 = spk2.cuda()
            env = env.cuda()

        with torch.no_grad():
            # SDR as objective
            if not args.use_pit:
                clean = spk1
                recon = model([feat, env], args.noise_test)
                loss = - sSDR(recon, clean)
            else:
                clean1, clean2 = spk1, spk2
                recon1, recon2 = model([feat, env], args.noise_test)

                recon = torch.cat([recon1.unsqueeze(1), recon2.unsqueeze(1)], 1)
                clean = torch.cat([clean1.unsqueeze(1), clean2.unsqueeze(1)], 1)

                loss = - torch.mean(batch_SDR_torch(recon, clean, args.cuda))

        validation_loss += loss.data.item() * CONST

        data_gen.set_description(f"Test  epoch: {epoch:3d} || SDR: {-validation_loss / (batch_id + 1):4.4f}")

    return validation_loss / (batch_id + 1)
