import csv
import json
import pickle as pkl

import numpy as np
import torch
from sep_eval import sep_eval as se
from tqdm import tqdm

from model_stft import InfoComMask

CONST = 10 * np.log10(np.exp(1))


class Evaluator(object):
    def __init__(self):
        
        self.model = None
        self.models = []
        
        self.measures = []
        self.name_measure = []
        
        self.datasets = []
        self.name_dataset = []
        self.corrs = []
        self.snrs = []
        self.sub_id = []

    def add_model(self, name):
        self.models.append(name)
    
    def add_measure(self, measure, name=None):
        if name is None:
            name = 'measure{}'.format(len(self.measures))
        self.name_measure.append(name)
        self.measures.append(measure)
    
    def add_dataset(self, mix, hint, s1, name=None, corrs=None, snrs=None, sub_id=None, mode='env'):
        if name is None:
            name = 'model{}'.format(len(self.models))
        if corrs is not None:
            self.corrs.append(corrs)
        else:
            self.corrs.append(np.zeros((len(mix))))

        if snrs is not None:
            self.snrs.append(snrs)
        else:
            self.snrs.append(np.zeros((len(mix))))

        if sub_id is not None:
            self.sub_id.append(sub_id)
        else:
            self.sub_id.append(np.zeros((len(mix))))

        self.name_dataset.append(name)
        self.datasets.append({'mix': mix, 'hint': hint, 's1': s1, 'mode': mode})

    @staticmethod
    def single_test(model, mix, env, mode):
        # doing batches of 4
        batch_size = 1
        out = []
        for i in tqdm(range(0, len(mix), batch_size)):
            _out = model([mix[i:i + batch_size].cuda(), env[i:i + batch_size].cuda()], branch=mode)
            _out = _out.cpu().data.numpy()
            out.append(_out)
        out = np.vstack(out)
        return out

    @staticmethod
    def load_info_model(load, base_dir='/Data/Dropbox/PhD/Projects/brain-informed-source-separation'):
        json_dir = base_dir + '/logs2/' + load
        with open(json_dir + '/architecture.json', 'r') as fff:
            p = json.load(fff)
            load_path = json_dir

            model = InfoComMask(n_fft=p['nfft'], kernel=(p['kf'], p['kt']), causal=p['causal'],
                                layers=p['layers'], stacks=p['stacks'], verbose=False)

            model.load_state_dict(torch.load(load_path + '/model_weights.pt'))
            model = model.cuda()
            _ = model.eval()
            return model, p

    def evaluate(self, file_path='./out.csv', mode='w'):
        
        with open(file_path, mode) as csv_file:
            file_writer = csv.writer(csv_file, delimiter=',')
            header = ['model', "nfft", "hop", "kernel1", "kernel2", "stacks", "nspk", "layers", "causal", "spkprob",
                      "tr_len", "te_len", "max_len", "noise_train", "frame_train", "noise_test", "frame_test",
                      "dataset_path", "dataset_name", "corrs", "snr", "sub_id"] + self.name_measure
            file_writer.writerow(header)
            for model in self.models:
                model_row = [model]

                print("Loading {}".format(model))
                _model, _p = self.load_info_model(model)

                for k, v in _p.items():
                    model_row.append(v)

                for dataset, name_dataset, corrs, snrs, sub_id in zip(self.datasets, self.name_dataset, self.corrs, self.snrs, self.sub_id):
                    dataset_row = [name_dataset]
                    _out = self.single_test(_model, dataset['mix'], dataset['hint'], dataset['mode'])
                    _s1 = dataset['s1'].cpu().data.numpy()
                    collected_measures = []
                    for measure, name_measure in zip(self.measures, self.name_measure):
                        collected_measures.append(measure(_out, _s1))
                    # separate
                    res = np.zeros((len(self.measures), len(collected_measures[0]), 4))
                    for j, m in enumerate(collected_measures):
                        for i, val in enumerate(m):
                            res[j, i, 0] = val
                            res[j, i, 1] = corrs[i]
                            res[j, i, 2] = snrs[i]
                            res[j, i, 3] = sub_id[i]
                    for i in range(res.shape[1]):
                        _t = []
                        _t.append(res[0, i, 1])
                        _t.append(res[0, i, 2])
                        _t.append(res[0, i, 3])
                        for j in range(res.shape[0]):
                            _t.append(res[j, i, 0])
                        file_writer.writerow(model_row + dataset_row + _t)
                del _model  # for RAM


def main():

    # Real data
    real_data = pkl.load(open('prepared_speech_vs_noise_eeg_v2.pkl', 'rb'))
    s1 = torch.from_numpy(real_data['s1'].astype('float32'))
    mix = torch.from_numpy(real_data['mix'].astype('float32'))
    env = torch.from_numpy(real_data['env'].astype('float32'))

    corrs = real_data['corrs']
    snrs = real_data['snrs']
    sub_id = real_data['sub_id']

    print("REAL test MIX: {}".format(mix.shape))
    print("REAL test ENV: {}".format(env.shape))
    print("REAL test s1: {}".format(s1.shape))

    # ACTUAL COMPUTE
    evaluator = Evaluator()

    # NC
    evaluator.add_model('201909022406_ICM_noise_15_12_dan_125_bab_NC')

    # C
    evaluator.add_model('201909015533_ICM_noise_15_12_dan_125_bab_C')

    evaluator.add_dataset(mix, env, s1, name='recon test env', mode='env', corrs=corrs, snrs=snrs, sub_id=sub_id)

    evaluator.add_measure(se.sdr, name='sdr')
    evaluator.add_measure(se.stoi, name='stoi')
    evaluator.evaluate(file_path='./final_evaluation_eeg_with_corrs_v2.csv')
    

if __name__ == "__main__":
    main()
