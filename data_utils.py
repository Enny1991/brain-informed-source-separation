import random

import h5py
import numpy as np
import torch
from scipy.io import wavfile
from scipy.signal import resample_poly as resample, hilbert
from torch.utils.data import Dataset

import filterbanks as fb


def simple_low_pass2(x, frame_step=40, noise=0.0):
    x = np.hstack([np.zeros((frame_step,)), x])

    low_pass = np.abs(x) ** 0.3
    low_pass = resample(low_pass, 1, frame_step)
    low_pass = (low_pass - np.min(low_pass)) / np.max(low_pass - np.min(low_pass))
    # fit noise
    low_pass += np.random.randn(len(low_pass)) * noise

    low_pass = np.concatenate([low_pass, np.zeros((1,))])

    return low_pass


def noise_lp(x, frame_len=20, noise=0.0):
    l = len(x)
    frame_len = np.random.choice(np.append(np.arange(2, frame_len + 1, 2), 1))  # uniform up to maximum
    
    x = np.hstack([np.zeros((frame_len//2,)), x, np.zeros((frame_len//2,))])
    x += np.random.randn(len(x)) * (np.random.uniform() * noise)  # uniform up to maximum
    lp = np.zeros((l,))
    n_win = len(x) - frame_len
    
    for i in range(n_win):
        lp[i] = np.mean(x[i:i + frame_len])
    return lp


def noise_only(x, noise=0.0):
    lp = x + np.random.randn(len(x)) * noise
    return lp
    
    
def norm_power(sig):
    sig -= np.mean(sig, -1, keepdims=True)
    power = np.std(sig, -1, keepdims=True)
    sig /= power
    return sig


def daniel_env(x, audio_fs=8000, hop=125):
    # audio_fs / hop = sampling freq of envelope
    N = 31  # number of channels / filters
    low_lim = 80  # centre freq. of lowest filter
    high_lim = 8000  # centre freq. of highest filter
    leny = x.shape[0]  # filter bank length

    erb_bank = fb.EqualRectangularBandwidth(leny, audio_fs, N, low_lim, high_lim)
    erb_bank.generate_subbands(x)

    erb_subbands = erb_bank.subbands[:, 1:-1]

    erb_envs = np.transpose(np.absolute(hilbert(np.transpose(erb_subbands)))) ** 0.3

    my_env = np.mean(erb_envs, 1)
    my_env = resample(my_env, 1, hop)
    
    return my_env


def envelope(x, frame_step=125, compression=0.3):
    x = np.hstack([np.zeros((frame_step,)), x])
    low_pass = np.abs(x) ** compression
    low_pass = resample(low_pass, 1, frame_step)
    return low_pass


def load_wav(filename, max_len=None, fsample=8000):
    old_fs, w = wavfile.read(filename)
    if len(w.shape) > 1:
        w = w[:, 0]  # only channel 0
    w = w.astype('float32')

    if max_len is not None:
        if max_len < w.shape[0]:
            i = np.random.randint(0, len(w) - max_len - 1)
            w = w[i:i + max_len + 1]

    if old_fs != fsample:
        if old_fs > fsample:
            factor = old_fs // fsample
            w = resample(w, 1, factor)
        else:
            factor = fsample // old_fs
            w = resample(w, factor, 1)

    return true_SNR(w)


def true_SNR(sig, snr_range=(-2.5, 2.5)):
    snr = np.random.rand() * (snr_range[1] - snr_range[0]) + snr_range[0]
    sig = unit_power(sig)
    factor = np.sqrt(10 ** (snr / 10.))
    sig *= factor
    return sig


def unit_power(sig):
    sig -= np.mean(sig)
    sig /= np.sqrt(np.mean(sig ** 2.)) + 1e-12
    return sig


def norm_power_wav(sig):
    sig -= np.mean(sig)
    power = np.max(np.abs(sig))
    sig /= power + 1e-12
    return sig


class DatasetSTFTv5(Dataset):
    """ Dataset Wrapper: Creates mixtures on the fly from the given h5 dataset """
    def __init__(self, path, n_fft=256, hop=125, seed=42,
                 task='train', n_spk=2, n_spk_prob=False, max_len=32000, noise=0.0, frame_len=1, llen=20000,
                 use_pit=False, env="std"):
        super(DatasetSTFTv5, self).__init__()

        random.seed(seed)
        np.random.seed(seed)
        self.task = task
        self.n_spk = n_spk
        self.n_spk_prob = n_spk_prob
        
        self.max_len = max_len
        self.__noise = noise
        self.__frame_len = frame_len
        self.n_fft = n_fft
        self.hop = hop
        
        self.h5pyLoader = h5py.File(path, 'r')
        self.singles = self.h5pyLoader[task]
        self.n_singles = self.singles.shape[0]
        self.len = llen
        self.use_pit = use_pit
        self.env = env

    def get_noise(self):
        return self.__noise

    def set_noise(self, noise):
        self.__noise = noise

    def get_frame_len(self):
        return self.__frame_len

    def set_frame_len(self, frame_len):
        self.__frame_len = frame_len

    def __getitem__(self, index):
        
        if self.n_spk_prob:
            n_spk = np.random.randint(2, self.n_spk + 1)
        else:
            n_spk = self.n_spk
           
        choice = np.random.choice(np.arange(self.n_singles), n_spk, replace=False)

        y = [true_SNR(self.singles[_y]) for _y in choice]
        
        # Mix signals
        mx = np.sum(np.array(y), axis=0)
        mx = norm_power_wav(mx)
        y0 = y[0]

        # envelope
        if self.env == "std":
            raw_gt = y0[:self.max_len]
            raw_gt = envelope(raw_gt, frame_step=self.hop)
        elif self.env == "dan":
            raw_gt = y0[:self.max_len]
            raw_gt /= np.sqrt(np.mean((raw_gt - np.mean(raw_gt)) ** 2)) + 1e-15
            raw_gt *= 0.025
            raw_gt = daniel_env(raw_gt)
        else:
            raise ValueError("Env not implemented")

        mx = mx[:self.max_len]
        y0 = y0[:self.max_len]
        y1 = y[1][:self.max_len]
        raw_gt = raw_gt[:self.max_len // self.hop + 1]

        if len(y0) < self.max_len:
            y0 = np.concatenate([y0, np.zeros((self.max_len - len(y0),))])
            y1 = np.concatenate([y1, np.zeros((self.max_len - len(y1),))])
            mx = np.concatenate([mx, np.zeros((self.max_len - len(mx),))])

        if len(raw_gt) < self.max_len // self.hop + 1:
            raw_gt = np.concatenate([raw_gt, np.zeros((self.max_len // self.hop + 1 - len(raw_gt),))])

        raw_gt = norm_power_wav(raw_gt)
        raw_gt = noise_only(raw_gt, noise=self.get_noise()).astype(np.float32)
        
        mix_tensor = torch.from_numpy(mx.astype('float32'))
        h_tensor = torch.from_numpy(raw_gt.astype('float32'))
        s1_tensor = torch.from_numpy(y0.astype('float32'))
        s2_tensor = torch.from_numpy(y1.astype('float32'))

        if not self.use_pit:
            return mix_tensor, s1_tensor, h_tensor
        else:
            return mix_tensor, s1_tensor, s2_tensor, h_tensor

    def __len__(self):
        return self.len


class DatasetSTFTv5BAB(Dataset):
    """ Danish/English Dataset for babble noise """
    def __init__(self, path, n_fft=256, hop=125, seed=42,
                 task='train', n_spk=2, n_spk_prob=False, max_len=32000, noise=0.0, frame_len=1, llen=20000,
                 use_pit=False, env="std"):
        super(DatasetSTFTv5BAB, self).__init__()

        random.seed(seed)
        np.random.seed(seed)
        self.task = task
        self.n_spk = n_spk
        self.n_spk_prob = n_spk_prob

        self.max_len = max_len
        self.__noise = noise
        self.__frame_len = frame_len
        self.n_fft = n_fft
        self.hop = hop

        self.h5pyLoader = h5py.File(path, 'r')
        self.singles = self.h5pyLoader[task]
        self.n_singles = self.singles.shape[0]
        self.nnoise = np.load('/work3/jhjort/biss/babble_noise_train.npy') if task == 'train' \
            else np.load('/work3/jhjort/biss/babble_noise_valid.npy')
        self.len = llen
        self.use_pit = use_pit
        self.env = env

    def get_noise(self):
        return self.__noise

    def set_noise(self, noise):
        self.__noise = noise

    def get_frame_len(self):
        return self.__frame_len

    def set_frame_len(self, frame_len):
        self.__frame_len = frame_len

    def __getitem__(self, index):

        choice_spk = np.random.choice(np.arange(self.n_singles), 1, replace=False)[0]
        _a = true_SNR(self.singles[choice_spk], snr_range=(0, 0))

        idx_noise = np.random.randint(len(self.nnoise) - len(_a) - 1)
        _noise = self.nnoise[idx_noise:idx_noise + len(_a)]

        _b = true_SNR(_noise, snr_range=(-10, 3))
        y = [_a, _b]

        # Mix signals
        mx = np.sum(np.array(y), axis=0)
        mx = norm_power_wav(mx)
        y0 = y[0]

        # envelope
        if self.env == "std":
            raw_gt = y0[:self.max_len]
            raw_gt = envelope(raw_gt, frame_step=self.hop)
        elif self.env == "dan":
            raw_gt = y0[:self.max_len]
            raw_gt /= np.sqrt(np.mean((raw_gt - np.mean(raw_gt)) ** 2)) + 1e-15
            raw_gt *= 0.025
            raw_gt = daniel_env(raw_gt)
        else:
            raise ValueError("Env not implemented")

        mx = mx[:self.max_len]
        y0 = y0[:self.max_len]
        y1 = y[1][:self.max_len]
        raw_gt = raw_gt[:self.max_len // self.hop + 1]

        if len(y0) < self.max_len:
            y0 = np.concatenate([y0, np.zeros((self.max_len - len(y0),))])
            y1 = np.concatenate([y1, np.zeros((self.max_len - len(y1),))])
            mx = np.concatenate([mx, np.zeros((self.max_len - len(mx),))])

        if len(raw_gt) < self.max_len // self.hop + 1:
            raw_gt = np.concatenate([raw_gt, np.zeros((self.max_len // self.hop + 1 - len(raw_gt),))])

        # noise
        _rnd_noise = np.random.rand(1) * self.get_noise()
        _choices = np.arange(2, self.get_frame_len() + 2, 2)
        np.append(_choices, 1)
        _rnd_frame = np.random.choice(_choices)
        raw_gt = noise_lp(raw_gt, frame_len=_rnd_frame, noise=_rnd_noise).astype(np.float32)

        mix_tensor = torch.from_numpy(mx.astype('float32'))
        h_tensor = torch.from_numpy(raw_gt.astype('float32'))
        s1_tensor = torch.from_numpy(y0.astype('float32'))
        s2_tensor = torch.from_numpy(y1.astype('float32'))

        if not self.use_pit:
            return mix_tensor, s1_tensor, h_tensor
        else:
            return mix_tensor, s1_tensor, s2_tensor, h_tensor

    def __len__(self):
        return self.len
